class Item{
    x = 0;
    y = 0;
    attraper = 0;
    taillex = 20;
    tailley = 20;

    constructor(pointX, pointY)
    {
        this.x = pointX;
        this.y = pointY;
    }

    catch()
    {
        this.attraper = 1;
    }

    collision(snakex, snakey, snakeh, snakel)
    {
        if (this.x > snakex + snakel
            || this.x < snakex - snakel
            || this.y > snakey + snakeh
            || this.y < snakey - snakeh ) {
            return false;
        } else {
            return true;
        }
    }
}