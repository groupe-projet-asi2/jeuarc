class Snake {

    bonus = 0;
    x = 0;
    y = 0;
    queue = 1;
    
    constructor(pointX, pointY) {
        this.x = pointX;
        this.y = pointY;
    }

    deplacement(pointX, pointY) {
        this.x += pointX;
        this.y += pointY;
    }

    setBonus(bonus)
    {
        this.bonus += bonus;
    }
}
